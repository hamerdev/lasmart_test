require('./scss/main.scss');

var inputUploader = document.getElementById('i_upload'),
    imageWrap = document.querySelector('.plan'),
    paramsForm = document.querySelector('.params-form');


inputUploader.addEventListener('change', getImage);
paramsForm.addEventListener('submit', validateForm);

function getImage(e) {
    var freader = new FileReader(),
        file = e.target.files[0],
        uploadText = document.querySelector('.upload__text');

    document.querySelector('.rectangle').style.display = 'none';

    if(!file.type.match('image/*')) {
        uploadText.innerHTML = 'Файл не поддерживается, загрузите изображение';
        uploadText.classList.add('upload__text--failed');
        return;
    } else {
        uploadText.classList.remove('upload__text--failed');
    }

    freader.onload = function (event) {
        uploadText.innerHTML = file.name;
        imageWrap.innerHTML = '<img class="plan__img" src="' + event.target.result + '">';
    };
    freader.onerror = function () {
        uploadText.innerHTML = 'Во время загрузки произошла ошибка. Попробуйте снова.';
    };
    freader.readAsDataURL(e.target.files[0]);
}

function validateForm(e) {
    e.preventDefault();
    var inputs = e.target.querySelectorAll('.form__input'),
        userWidth = e.target.elements.width.value,
        userHeight = e.target.elements.height.value,
        image = document.querySelector('.plan__img'),
        uploadText = document.querySelector('.upload__text'),
        errorsCount = 0;

    Array.prototype.forEach.call(inputs, function (input) {
        var inputHelper = input.parentNode.nextElementSibling;

        if(!(/^\d+$/).test(input.value)) {
            inputHelper.innerHTML = 'Значение должно быть числом';
            errorsCount++;
        } else if(input.value < 5000) {
            inputHelper.innerHTML = 'Значение должно быть больше 5000 мм';
            errorsCount++;
        } else {
            inputHelper.innerHTML = '';
        }
    });


    if(!inputUploader.files[0] || !inputUploader.files[0].type.match('image/*')) {
        uploadText.innerHTML = 'Выберите файл';
        uploadText.classList.add('upload__text--failed');
        errorsCount++;
    } else {
        uploadText.classList.remove('upload__text--failed');
    }

    if (image && !errorsCount) {
        var rectWidth = (image.width / userWidth) * 1000,
            rectHeight = (image.height / userHeight) * 500,
            rect = document.querySelector('.rectangle');
        rect.style.height = rectHeight + "px";
        rect.style.width = rectWidth + "px";
        rect.style.display = 'block';
        rect.style.marginLeft = -rectWidth / 2 + "px";
        rect.style.marginTop = -rectHeight / 2 + "px";
    }

}

